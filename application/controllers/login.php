<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	
	public function index()
	{
		$this->load->model('admin_model');
		if($this->admin_model->is_admin_logged_in()){


		redirect('dashboard');
	}
	else{
		$this->load->view('index_page');
	}

	}
	

	public function admin_login_data_check()
	{
		$this->form_validation->set_rules('email','email','required|trim|xss_clean');
	    $this->form_validation->set_rules('password','password','required|trim|min_length[5]');
	     $this->form_validation->set_rules('remember_me','remember_me');
       


if ($this->form_validation->run() == FALSE){
	$this->load->view('index_page');
	}
	else
	{
		$this->load->model('Admin_model');
		$result = $this->Admin_model->admin_login_data_check();
	


	if($result){
		redirect('dashboard');
	}
	else{
		$data['errorlogin'] = 'email and password is invalid';
		$this->load->view('index_page', $data);
	}

     


}
}
// logout
public function logout(){
	$this->session->unset_userdata('current_id');
	$this->session->unset_userdata('current_email');
	$this->session->sess_destroy();
	redirect('login/?logout_success');


}
}