<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {
	 public function __construct(){
	 	parent:: __construct(); 
	 	$this->load->model('Employee_model');
	if(!$this->Employee_model->is_employee_logged_in() ){
		redirect('login/?logged_in_first');
	}
}

 
	public function index(){
	
redirect('dashboard');
} 
	





public function total_Employee(){
	$this->load->model('Employee_model');
	$data['Employees']=$this->Employee_model->view_all();
	$this->load->view('total_Employee',$data);
}



     public function add_employee(){
	 // $data['addadmin']='addadmin';
		$this->load->view('add_employee');
 

	}

	public function add_Employee_data_check(){
$this->form_validation->set_rules('fullname','Name','required|trim|xss_clean|min_length[3]');

$this->form_validation->set_rules('post','Post','required|trim|xss_clean|min_length[3]');

$this->form_validation->set_rules('dob','Date Of Birth','required|trim|min_length[8]');
$this->form_validation->set_rules('email','Email','required|trim|xss_clean|min_length[9]');

$this->form_validation->set_rules('getid','GET ID','required|trim|min_length[5]');

$this->form_validation->set_rules('mac','Mac Address','required|trim|min_length[5]');
//$this->form_validation->set_rules('Password2','Retype Password','required|trim|xss_clean|min_length[3]|matches
  //    [password]');
	 

if ($this->form_validation->run() == FALSE){
	$this->load->view('add_employee');
	
}
else

{
	$this->load->model('Employee_model');
	if($this->Employee_model->add_Employee_data_insert() ):

		redirect('employee/total_Employee');
	else:
		$data='something error';
	$this->load->view('dashboard_layout',$data);
	endif;

	
}
}


//edite
//public function update_employee(){
	
//$this->load->view('employee/update_employee');
//}




#edit employee datachck

	public function update_employee(){
	$this->load->model('Employee_model');
	$data['Employees']=$this->Employee_model->view_all();
	$this->load->view('update_employee');
}

public function edit_employee_data_check(){
$this->form_validation->set_rules('fullname','Name','required|trim|xss_clean|min_length[3]');

$this->form_validation->set_rules('post','Post','required|trim|xss_clean|min_length[3]');

$this->form_validation->set_rules('dob','Date Of Birth','required|trim|min_length[8]');
$this->form_validation->set_rules('email','Email','required|trim|xss_clean|min_length[9]');

$this->form_validation->set_rules('getid','GET ID','required|trim|min_length[5]');

$this->form_validation->set_rules('mac','Mac Address','required|trim|min_length[5]');
	 
  
if ($this->form_validation->run() == FALSE){
	
	
	$this->load->view('update_employee');
	
}
else

{
	$this->load->model('Employee_model');
	if($this->Employee_model->edit_employee_data_Update() ):

		redirect('employee/total_Employee');
	else:
		$data='something error';
	$this->load->view('update_employee',$data);
	endif;

	
}
}
}




	

