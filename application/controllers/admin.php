<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	 public function __construct(){
	 	parent:: __construct(); 
	 	$this->load->model('Admin_model');
	if(!$this->Admin_model->is_admin_logged_in() ){
		redirect('login/?logged_in_first');
	}
}

 
	public function index(){
	
redirect('dashboard');
} 
	




	

public function total_Admin(){
		
	

$this->load->library('pagination');

	
	$config['base_url'] = "http://localhost/test/admin/total_admin";
	$config['per_page'] = 2;
	$config['limit'] = 5;
	$config['limit_start'] = 0;
	$config['num_links'] = 3;
	$config['total_rows'] = $this->db->get('add_admin_tbl')->num_rows();

	//$config['total_rows'] = $this->results->num_rows();
	
	$this->pagination->initialize($config);
	$data['Admin'] = $this->db->get('add_admin_tbl', $config['per_page']=2,$config['num_links'] =3, 
		$config['limit'] = 5,$config['limit_start'] = 0);
	$data['links'] = $this->pagination->create_links();
	//$this->load->view('total_admin',$data);
	$this->load->model('Admin_model');
	$data['Admins']=$this->Admin_model->view_all();
	$this->load->view('total_Admin',$data);
	
	


}

	

     public function add_admin(){
	
		$this->load->view('add_admin');
 

	}

	public function add_admin_data_check(){
$this->form_validation->set_rules('fullname','Full Name','required|trim|xss_clean|min_length[3]');
$this->form_validation->set_rules('dob','Date Of Birth','required|trim|min_length[8]');
$this->form_validation->set_rules('email','email','required|trim|xss_clean|min_length[9]');
$this->form_validation->set_rules('password','Password','required|trim|min_length[5]');
//$this->form_validation->set_rules('password2','Retypepassword','required|trim|xss_clean|min_length[5]|matches
      //[password]');
	 
  
if ($this->form_validation->run() == FALSE){
	$this->load->view('add_admin');
	
}
else

{
	$this->load->model('Admin_model');
	if($this->Admin_model->add_admin_data_insert() ):

		redirect('admin/total_admin');
	else:
		$data['errorInsert']='something error';
	$data['add_admin']='add_admin';
	$this->load->view('dashboard_layout',$data);
	endif;


	


	
}
}

//delete
public function Admin_delete($id=null){

	
//echo "delete process-".$id;
		$this->load->model('Admin_model');
	if ($this->Admin_model->admin_delete($id)){
		redirect('admin/total_admin');

}
else{
	redirect ('dashboard');
}
}

//edite
public function Admin_edit($id=null){
 $this->load->model('Admin_model');
if ($this->Admin_model->admin_edit($id)){
	$data['admininfo']=$this->Admin_model->admin_edit($id);
}
	$data['admin_edit'] = 'admin_edit';
	
$this->load->view('admin_edit',$data);
}




#edit admin datachck

public function admin_edit_data_check($id=null){
$this->form_validation->set_rules('fullname','Full Name','required|trim|xss_clean|min_length[3]');
$this->form_validation->set_rules('dob','Date Of Birth','required|trim|min_length[8]');
$this->form_validation->set_rules('email','email','required|trim|xss_clean|min_length[9]');
//$this->form_validation->set_rules('password','Password','required|trim|min_length[5]');
//$this->form_validation->set_rules('password2','Retypepassword','required|trim|xss_clean|min_length[5]|matches
      //[password]');
	 
  
if ($this->form_validation->run() == FALSE){
	if ($this->Admin_model->admin_edit($id)){
	$data['admininfo']=$this->Admin_model->admin_edit($id);
}

	$data['admin_edit'] = 'admin_edit';
	$this->load->view('admin_edit',$data);
	
}
else

{
	$this->load->model('Admin_model');
	if($this->Admin_model->admin_edit_data_Update($id) ):

		redirect('admin/total_admin');
	else:
		if ($this->Admin_model->admin_edit($id)){
	$data['admininfo']=$this->Admin_model->admin_edit($id);
}
		$data['errorupdate']='something error';
	$data['admin_edit'] = 'admin_edit';
	
$this->load->view('admin_edit',$data);
	endif;
}
	
}
}


	





