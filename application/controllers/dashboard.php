<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	
	public function index(){
		$this->load->model('admin_model');
		if($this->admin_model->is_admin_logged_in()){


		$this->load->view('dashboard_layout');
		}


//echo " name:  ". $this->session->userdata('current_email');
		else{
			redirect('login/?logged_in_first');
		}

	}

} 