<?php include 'main.php'; ?>


<div class="col-lg-1"></div>


	<?php if(validation_errors()){
     	   ?>
     	   <div class="col-lg-1">
     	   <div class="alert">
     	     <?php echo validation_errors(); ?>
     	   </div>
     	   </div>
     	   <?php
     	}
     	?>

    <div class="container">
    <div class="row">
            	<?php if(isset($errorupdate)){
     	   ?>
     	   <div class="col-lg-1">
     	   <div class="alert">
     	     <?php echo $errorupdate; ?>
     	   </div>
     	   </div>
     	   <?php
     	}
     	?>
   

     <div class="row">
     	<div class="col-lg-2"></div>
     	<div class="col-lg-8 employee_regi">
     	
     	 <?php

				 if($admininfo):
     				foreach($admininfo as $admin):   

		     echo form_open('admin/admin_edit_data_check/'.$admin->id);
     	                                                  ?>

		      	<h2 class="form-signin-heading">Update Admin</h2>
				  <div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
				    <div class="col-sm-10">

				   
				    		
				    <?php	
				    
				     $fullname = array(
		        	'id'     => 'inputEmail3',
		        	'name'   => 'fullname',
		        	'class'  => 'form-control',
		        	'required' => 'required',
		        	
		        	'value'=> $admin->fullname

                       );
		           echo form_input($fullname);
		           ?>
				    		
				    
				     
				    </div>
				  </div>

				  

				  <div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">Date of Birth</label>
				    <div class="col-sm-10">
				        <?php

		         $dob = array(
		         	'id'     => 'date',
		        	
		        	'name'   => 'dob',
		        	'class'  => 'form-control datepicker',
		        	'required' => 'required',
		        	
		        	'value' => $admin->dob
                       );
		           echo form_input($dob);
		           ?>
		        

				      
				    </div>
				  </div>
				      
				   

				

				  <div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
				    <div class="col-sm-10">
				     <?php

				    $email = array(
		        	'id'     => 'inputEmail3',
		        	'name'   => 'email',
		        	'class'  => 'form-control',
		        	'required' => 'required',
		        	
		        	'value' => $admin->email
                       );

				  endforeach;
				 endif;
		           echo form_input($email);
		           ?>
				     
				    </div>
				  </div> 



				  
				  <div class="form-group">
				    <div class="col-sm-offset-2 col-sm-10">
				      <button type="submit" class="btn btn-success">Update</button>
				    </div>
				  </div>

				 <?php echo form_close(); ?>

     	</div>
     	<div class="col-lg-2"></div>
     </div>

    </div> <!-- /container -->
   





	

       