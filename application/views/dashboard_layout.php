<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Gradient Employee Tracker</title>
	<link rel="stylesheet" href="<?php echo base_url() ?>libs/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>libs/css/datepicker.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>libs/css/style.css">
</head>
<body>





<section id="navigation-menu">
	
		<nav class="navbar navbar-default navbar-inverse">
		  <div class="container-fluid">
		 
		



		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">GIT</a>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li class="active"><a href="<?php echo base_url() ?>dashboard">Home <span class="sr-only">(current)</span></a></li>
		        <li class="dropdown">
		          <a href="<?php echo base_url() ?>dashboard" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Employee <span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="employee/add_employee">Add Employee</a></li>
		            <li><a href="employee/total_employee">Total Employee</a></li>
		            <li><a href="employee/update_employee">Update Employee</a></li>
		          </ul>
		        </li>
		        <li class="dropdown">
		          <a href="<?php echo base_url() ?>dashboard" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admin <span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="admin/add_admin">Add Admin</a></li>
		            <li><a href="admin/total_admin">Total Admin</a></li>
		          </ul>
		        </li>

		           
		        <li><a href="#">Present Status</a></li>
		       
		        <li><a href="login/logout">Logout</a></li>


		      </ul>

		 
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</section>





	
  




<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="<?php echo base_url() ?>libs/js/bootstrap.js"></script>
	<script src="<?php echo base_url() ?>libs/js/bootstrap-datepicker.js"></script>
	<script>
		$('.datepicker').datepicker()

	</script>

</body>
</html>