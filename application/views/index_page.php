

	
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Gradient Employee Tracker</title>
	<link rel="stylesheet" href="<?php echo base_url() ?>libs/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>libs/css/datepicker.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>libs/css/style.css">
</head>
<body>


    <div class="container">

     <div class="row">

     	<div class="col-lg-4"></div>

     	<div class="col-lg-4 main-admin-login">
     	


     		    <?php


	           echo form_open('login/admin_login_data_check');?> 

		        <h2 class="form-signin-heading">Please sign in</h2>
		        <div class= "col s12">
		        <?php
		        if(validation_errors()){
		        	?>
		        	<div class= "alert">
		        	
		        	<?php echo validation_errors();?>
		        	</div>
		        	<?php
		        	}
		        
		       

		           if(isset($errorlogin)):
		           	 ?>
		        
		        <div class="alert">
		        	<?php echo $errorlogin;?>
		        	
		        	</div>
		        <?php endif;
		        ?>
		        </div>



		        
           
		        <label for="inputemail" class="sr-only">email</label>
		        <?php

		         $email = array(


		        	'id'     => 'inputemail',
		        	'name'   => 'email',
		        	'class'  => 'form-control',
		        	'required' => 'required',
		        	'placeholder' => 'email',
                       );
		           echo form_input($email);
		           ?>
		        


		        </br> 
		        <label for="inputpassword" class="sr-only">password</label>
		        <?php

		         $password = array(
		        	'id'     => 'inputPassword',
		        	'name'   => 'password',
		        	'class'  => 'form-control',
		        	'required' => 'required',
		        	'placeholder' => 'password',
                       );
		           echo form_password($password);
		           ?>


		        
		        <div class="checkbox">
		          <label>
		           <?php

		         $remember_me = array(
		        	'id'     => 'inputcheckbox',
		        	'name'   => 'remember_me',
		        	'class'  => 'checkbox',
		        
		        	//'value'=>'remember_me',
		        	//'required' => 'required',

		        	  );
		           echo form_checkbox($remember_me);
		           ?>

		         remember_me
		         
		          </label>
		        </div>
		        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
		       <? php echo form_close(); ?>

     	</div>
     	<div class="col-lg-4"></div>
     </div>

    </div> <!-- /container -->





	      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="<?php echo base_url() ?>libs/js/bootstrap.js"></script>
	<script src="<?php echo base_url() ?>libs/js/bootstrap-datepicker.js"></script>
	<script>
		$('.datepicker').datepicker()

	</script>

</body>
</html>    