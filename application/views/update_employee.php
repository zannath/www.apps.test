<?php include 'main.php'; ?>



	<div class="col-lg-1"></div>


	<?php if(validation_errors()){
     	   ?>
     	   <div class="col-lg-1">
     	   <div class="alert">
     	     <?php echo validation_errors(); ?>
     	   </div>
     	   </div>
     	   <?php
     	}
     	?>

     	

    <div class="container">
      <div class="row">
            	<?php if(isset($errorupdate)){
     	   ?>
     	   <div class="col-lg-1">
     	   <div class="alert">
     	     <?php echo $errorupdate; ?>
     	   </div>
     	   </div>
     	   <?php
     	}
     	?>
   

     <div class="row">
     	<div class="col-lg-2"></div>
     	<div class="col-lg-8 employee_regi">
     	
		      <?php echo form_open('employee/edit_employee_data_check');?>
		      	<h2 class="form-signin-heading">Update Employee</h2>
				  <div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
				    <div class="col-sm-10">
				    <?php

		         $fullname= array(
		        	'id'     => 'inputEmail3',
		        	'name'   => 'fullname',
		        	'class'  => 'form-control',
		        	'required' => 'required',
		        	'placeholder' => 'Full Name',
                       );
		           echo form_input($fullname);
		           ?>
				      
				    </div>
				  </div>

				  <div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">Post</label>
				    <div class="col-sm-10">
				     <?php

		         $post = array(
		        	'id'     => 'inputEmail3',
		        	'name'   => 'post',
		        	'class'  => 'form-control',
		        	'required' => 'required',
		        	'placeholder' => 'Post',
                       );
		           echo form_input($post);
		           ?>
		        

				      
				    </div>
				  </div>

				  <div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">Date of Birth</label>
				    <div class="col-sm-10">
				     <?php

		         $dob = array(
		        	'id'     => 'date',
		        	'name'   => 'dob',
		        	'class'  => 'form-control datepicker',
		        	'required' => 'required',
		        	'placeholder' => '6/10/1994',
		        	
                       );
		           echo form_input($dob);
		           ?>
				     
				    </div>
				  </div>

				

				  <div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
				    <div class="col-sm-10">
				     <?php

		         $email = array(
		        	'id'     => 'inputEmail3',
		        	'name'   => 'email',
		        	'class'  => 'form-control',
		        	'required' => 'required',
		        	'placeholder' => 'Email',
                       );
		           echo form_input($email);
		           ?>
				      
				    </div>
				  </div>

				  <div class="form-group">
				    <label for="inputPassword3" class="col-sm-2 control-label">GET ID</label>
				    <div class="col-sm-10">
				    <?php

		         $getid = array(
		        	'id'     => 'inputPassword3',
		        	'name'   => 'getid',
		        	'class'  => 'form-control',
		        	'required' => 'required',
		        	'placeholder' => 'GET ID',
                       );
		           echo form_input($getid);
		           ?>
				      
				    </div>
				  </div>

				  <div class="form-group">
				    <label for="inputPassword3" class="col-sm-2 control-label">Mac Address</label>
				    <div class="col-sm-10">

				     <?php

		         $mac = array(
		        	'id'     => 'inputPassword3',
		        	'name'   => 'mac',
		        	'class'  => 'form-control',
		        	'required' => 'required',
		        	'placeholder' => 'Mac Address',
                       );
		           echo form_input($mac);
		           ?>
				     
				    </div>
				  </div>

				  
				  <div class="form-group">
				    <div class="col-sm-offset-2 col-sm-10">
				      <button type="submit" class="btn btn-success">Update</button>
				    </div>
				  </div>
			<?php echo form_close(); ?>

     	</div>
     	<div class="col-lg-2"></div>
     </div>

    </div> <!-- /container -->





	
       