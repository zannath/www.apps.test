<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {

public function index()
	{
		$this->load->view('dashboard');

	}

	public function update_Employee(){
	$this->load->model('Employee_model');
	$data['Employees']=$this->Employee_model->view_all();
	$this->load->view('update_Employee',$data);
}



public function total_Employee(){
	$this->load->model('Employee_model');
	$data['Employees']=$this->Employee_model->view_all();
	$this->load->view('total_Employee',$data);
}



     public function add_employee(){
	 // $data['addadmin']='addadmin';
		$this->load->view('add_employee');
 

	}

	public function add_Employee_data_check(){
$this->form_validation->set_rules('fullname','Name','required|trim|xss_clean|min_length[3]');

$this->form_validation->set_rules('post','Post','required|trim|xss_clean|min_length[3]');

$this->form_validation->set_rules('dob','Date Of Birth','required|trim|min_length[8]');
$this->form_validation->set_rules('email','Email','required|trim|xss_clean|min_length[9]');

$this->form_validation->set_rules('getid','GET ID','required|trim|min_length[5]');

$this->form_validation->set_rules('mac','Mac Address','required|trim|min_length[5]');
//$this->form_validation->set_rules('Password2','Retype Password','required|trim|xss_clean|min_length[3]|matches
  //    [password]');
	 

if ($this->form_validation->run() == FALSE){
	$this->load->view('add_employee');
	
}
else

{
	$this->load->model('Employee_model');
	if($this->Employee_model->add_Employee_data_insert() ):

		redirect('dashboard');
	else:
		$data='something error';
	$this->load->view('dashboard_layout',$data);
	endif;

	
}
}
}


	

