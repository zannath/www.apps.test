<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	
	public function index()
	{
		$this->load->view('index_page');

	}
	public function admin_login_validation()
	{
		
		$this->form_validation->set_rules('email','email','required|trim|xss_clean');
	    $this->form_validation->set_rules('password','password','required|trim');
       


if ($this->form_validation->run() == FALSE){
	$this->load->view('index_page');
	
}
else

{
	$this->load->model('admin_model');
	$result=$this->admin_model->admin_login_validation();
	
	
	if($result){
		redirect('dashboard');
	}
	else{
		$data['errorlogin'] = 'email and password is invalid';
		$this->load->view('index_page', $data);
	}


	
}
}
/*
//logout
public function logout(){
	$this->session->unset_admindata('current_admin_id');
	$this->session->unset_admindata('current_admin_email');
	$this->session->sess_destroy();
	redirect('Login/?logout success');
}

*/
public function logout(){
	$this->session->sess_destroy();
	redirect('Login/?logout success');
}


}
