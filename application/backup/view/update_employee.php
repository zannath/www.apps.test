<?php include 'header.php'; ?>




	<section id="navigation-menu">
		
		<nav class="navbar navbar-default navbar-inverse">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">GIT</a>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li class="active"><a href="#">Home <span class="sr-only">(current)</span></a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Employee <span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="add_employee">Add Employee</a></li>
		            <li><a href="total_employee">Total Employee</a></li>
		            <li><a href="update_employee">Update Employee</a></li>
		          </ul>
		        </li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admin <span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="admin/add_admin">Add Admin</a></li>
		            <li><a href="admin/total_admin">Total Admin</a></li>
		          </ul>
		        </li>
		        <li><a href="#">Present Status</a></li>
		         <li><a href="<?php echo base_url() ?>dashboard">Refresh Page</a></li>
		        <li><a href="logout.php">Logout</a></li>
		      </ul>

		 
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</section>



    <div class="container">

     <div class="row">
     	<div class="col-lg-2"></div>
     	<div class="col-lg-8 employee_regi">
     	
		      <form class="form-horizontal">
		      	<h2 class="form-signin-heading">Update Employee</h2>
				  <div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
				    <div class="col-sm-10">
				      <input type="text" class="form-control" id="inputEmail3" placeholder="Full Name">
				    </div>
				  </div>

				  <div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">Post</label>
				    <div class="col-sm-10">
				      <input type="text" class="form-control" id="inputEmail3" placeholder="Post">
				    </div>
				  </div>

				  <div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">Date of Birth</label>
				    <div class="col-sm-10">
				      <input type="date" class="form-control datepicker" placeholder="6/10/1994">
				    </div>
				  </div>

				

				  <div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
				    <div class="col-sm-10">
				      <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
				    </div>
				  </div>

				  <div class="form-group">
				    <label for="inputPassword3" class="col-sm-2 control-label">GET ID</label>
				    <div class="col-sm-10">
				      <input type="text" class="form-control" id="inputPassword3" placeholder="GET ID">
				    </div>
				  </div>

				  <div class="form-group">
				    <label for="inputPassword3" class="col-sm-2 control-label">Mac Address</label>
				    <div class="col-sm-10">
				      <input type="text" class="form-control" id="inputPassword3" placeholder="Mac Address">
				    </div>
				  </div>

				  
				  <div class="form-group">
				    <div class="col-sm-offset-2 col-sm-10">
				      <button type="submit" class="btn btn-success">Update</button>
				    </div>
				  </div>
			  </form>

     	</div>
     	<div class="col-lg-2"></div>
     </div>

    </div> <!-- /container -->





	
       <?php include 'footer.php'; ?>    