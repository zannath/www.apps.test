<?php include 'header.php'; ?>




	<section id="navigation-menu">
		
		<nav class="navbar navbar-default navbar-inverse">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">GIT</a>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li class="active"><a href="#">Home <span class="sr-only">(current)</span></a></li>
		        <li class="dropdown">
		          <a href="dashboard" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Employee <span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="add_employee">Add Employee</a></li>
		            <li><a href="total_employee">Total Employee</a></li>
		            <li><a href="update_employee">Update Employee</a></li>
		          </ul>
		        </li>
		        <li class="dropdown">
		          <a href="dashboard" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admin <span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="add_admin">Add Admin</a></li>
		            <li><a href="total_admin">Total Admin</a></li>
		          </ul>
		        </li>
		        <li><a href="#">Present Status</a></li>
		         <li><a href="<?php echo base_url() ?>dashboard">Refresh Page</a></li>
		        <li><a href="logout.php">Logout</a></li>
		      </ul>

		 
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</section>

	<?php if(isset($errorInsert)){
     	   ?>
     	   <div class="col-lg-">
     	   <div class="alert">
     	     <?php echo $errorInsert; ?>
     	   </div>
     	   </div>
     	   <?php
     	}
     	?>

	



    <div class="container">

     <div class="row">
     	<div class="col-lg-2"></div>
     	<div class="col-lg-8 employee_regi">
     	
		    <?php echo form_open('employee/add_employee_data_check');?>
		      	<h2 class="form-signin-heading">Employee Registration</h2>
				  <div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
				    <div class="col-sm-10">
				     <?php

		         $fullname= array(
		        	'id'     => 'inputEmail3',
		        	'name'   => 'fullname',
		        	'class'  => 'form-control',
		        	'required' => 'required',
		        	'placeholder' => 'Full Name',
                       );
		           echo form_input($fullname);
		           ?>
		        
				    
				    </div>
				  </div>

				  <div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">Post</label>
				    <div class="col-sm-10">
				     <?php

		         $post = array(
		        	'id'     => 'inputEmail3',
		        	'name'   => 'post',
		        	'class'  => 'form-control',
		        	'required' => 'required',
		        	'placeholder' => 'Post',
                       );
		           echo form_input($post);
		           ?>
		        

				    </div>
				  </div>

				  <div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">Date of Birth</label>
				    <div class="col-sm-10">
				     <?php

		         $dob = array(
		        	'id'     => 'date',
		        	'name'   => 'dob',
		        	'class'  => 'form-control datepicker',
		        	'required' => 'required',
		        	'placeholder' => '6/10/1994',
		        	
                       );
		           echo form_input($dob);
		           ?>
		        
				    
				    </div>
				  </div>

				

				  <div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
				    <div class="col-sm-10">
				     <?php

		         $email = array(
		        	'id'     => 'inputEmail3',
		        	'name'   => 'email',
		        	'class'  => 'form-control',
		        	'required' => 'required',
		        	'placeholder' => 'Email',
                       );
		           echo form_input($email);
		           ?>
		        
				     
				   
				    </div>
				  </div>

				  <div class="form-group">
				    <label for="inputPassword3" class="col-sm-2 control-label">GET ID</label>
				    <div class="col-sm-10">
				     <?php

		         $getid = array(
		        	'id'     => 'inputPassword3',
		        	'name'   => 'getid',
		        	'class'  => 'form-control',
		        	'required' => 'required',
		        	'placeholder' => 'GET ID',
                       );
		           echo form_input($getid);
		           ?>
		        
				     
		
				    </div>
				  </div>

				  <div class="form-group">
				    <label for="inputPassword3" class="col-sm-2 control-label">Mac Address</label>
				    <div class="col-sm-10">
				     <?php

		         $mac = array(
		        	'id'     => 'inputPassword3',
		        	'name'   => 'mac',
		        	'class'  => 'form-control',
		        	'required' => 'required',
		        	'placeholder' => 'Mac Address',
                       );
		           echo form_input($mac);
		           ?>
		        
				     
				  
				    </div>
				  </div>

				  
				  <div class="form-group">
				    <div class="col-sm-offset-2 col-sm-10">
				      <button type="submit" class="btn btn-success">Submit</button>
				    </div>
				  </div>
			<? php echo form_close(); ?>

     	</div>
     	<div class="col-lg-2"></div>
     </div>

    </div> <!-- /container -->



<?php include 'footer.php'; ?>    
</html>