<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Gradient Employee Tracker</title>
	<link rel="stylesheet" href="<?php echo base_url() ?>libs/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>libs/css/datepicker.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>libs/css/style.css">
</head>
<body>